- dashboard: branch_scorecard
  title: Branch Scorecard
  layout: newspaper
  embed_style:
    background_color: "#ffffff"
    show_title: true
    title_color: "#3a4245"
    show_filters_bar: true
    tile_text_color: "#3a4245"
    text_tile_text_color: ''
  elements:
  - name: 'powered by'
    type: text
    body_text: <a href="https://keboola.com" target="_blank"> <img src="https://keboola-resources.s3.amazonaws.com/poweredByKeboola.png"
      width="100%"/>
    row: 0
    col: 18
    width: 6
    height: 2
  - name: 'dashboard'
    type: text
    subtitle_text: <font size="5px"><font color="#408ef7"><b>Overview</b></font>
    row: 0
    col: 7
    width: 11
    height: 2
  - name: 'scaffold'
    type: text
    subtitle_text: <font size="5px"><font color="#408ef7"><b>Paymo</b></font>
    row: 0
    col: 0
    width: 7
    height: 2
  - name: 'Branch by # of customers'
    title: 'Branch by # of customers'
    merged_queries:
    - model: block-keboola-bank_scorecard
      explore: customers
      type: looker_area
      fields: [customers.count, customers.branch_id]
      sorts: [customers.count desc]
      limit: 500
      series_types: {}
      hidden_fields: [customers.branch_id]
    - model: block-keboola-bank_scorecard
      explore: customers
      type: looker_column
      fields: [customers.branch_id, customers.count]
      sorts: [customers.count desc]
      limit: 500
      query_timezone: America/Los_Angeles
      series_types: {}
      join_fields:
      - field_name: customers.branch_id
        source_field_name: customers.branch_id
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: customers.count, id: customers.count,
            name: Customers}, {axisId: q1_customers.count, id: q1_customers.count,
            name: Customers}], showLabels: false, showValues: false, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: false
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    hide_legend: true
    legend_position: center
    series_types:
      q1_customers.count: column
    point_style: none
    series_colors:
      q1_customers.count: "#e31a1c"
      customers.count: "#27a686"
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    reference_lines: [{reference_type: line, line_value: mean, range_start: max, range_end: min,
        margin_top: deviation, margin_value: mean, margin_bottom: deviation, label_position: right,
        color: "#27a686", label: '', value_format: "##0"}]
    show_null_points: true
    interpolation: linear
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    type: looker_area
    hidden_fields:
    sorts: [customers.count desc]
    listen:
    -
    - Branch: branch.branch_address
    row: 12
    col: 5
    width: 7
    height: 4
  - title: Customer Count
    name: Customer Count
    model: block-keboola-bank_scorecard
    explore: customers
    type: single_value
    fields: [customers.count]
    limit: 500
    series_types: {}
    listen:
      Branch: branch.branch_address
    row: 12
    col: 0
    width: 5
    height: 4
  - title: Reviews By Rating
    name: Reviews By Rating
    model: block-keboola-bank_scorecard
    explore: reviews_topic
    type: looker_column
    fields: [reviews.count_reviews, reviews.review_rating]
    pivots: [reviews.review_rating]
    filters:
      reviews.source: ''
    sorts: [reviews.count_reviews desc 0, reviews.review_rating]
    limit: 500
    column_limit: 50
    query_timezone: America/Los_Angeles
    color_application:
      collection_id: legacy
      palette_id: legacy_diverging1
      options:
        steps: 5
        reverse: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: reviews.count_reviews,
            id: 1.0 - reviews.count_reviews, name: '1'}, {axisId: reviews.count_reviews,
            id: 2.0 - reviews.count_reviews, name: '2'}, {axisId: reviews.count_reviews,
            id: 3.0 - reviews.count_reviews, name: '3'}, {axisId: reviews.count_reviews,
            id: 4.0 - reviews.count_reviews, name: '4'}, {axisId: reviews.count_reviews,
            id: 5.0 - reviews.count_reviews, name: '5'}], showLabels: false,
        showValues: true, unpinAxis: false, tickDensity: default, tickDensityCustom: 5,
        type: linear}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: false
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    series_colors: {}
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    listen:
      Branch: branch.branch_address
    row: 5
    col: 8
    width: 8
    height: 5
  - name: Rating amongst peers
    title: Rating amongst peers
    merged_queries:
    - model: block-keboola-bank_scorecard
      explore: reviews_topic
      type: looker_column
      fields: [reviews.rating_value, branch.branch_address]
      filters:
        branch.branch_id: "-NULL"
      sorts: [reviews.rating_value desc]
      limit: 500
      column_limit: 50
    - model: block-keboola-bank_scorecard
      explore: reviews_topic
      type: table
      fields: [reviews.rating_value, branch.branch_address]
      sorts: [reviews.rating_value desc]
      limit: 500
      query_timezone: America/Los_Angeles
      join_fields:
      - field_name: branch.branch_address
        source_field_name: branch.branch_address
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: reviews.rating_value,
            id: reviews.rating_value, name: Reviews Response}, {axisId: q1_reviews.rating_value,
            id: q1_reviews.rating_value, name: Reviews Response}], showLabels: false,
        showValues: false, unpinAxis: false, tickDensity: default, tickDensityCustom: 5,
        type: linear}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: false
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    hide_legend: true
    legend_position: center
    series_types:
      q1_reviews.rating_value: column
    point_style: none
    series_colors:
      reviews.rating_value: "#27a686"
      q1_reviews.rating_value: "#ed6168"
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    reference_lines: [{reference_type: line, line_value: mean, range_start: max, range_end: min,
        margin_top: deviation, margin_value: mean, margin_bottom: deviation, label_position: right,
        color: "#27a686", label: ''}]
    show_null_points: false
    interpolation: linear
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    type: looker_area
    sorts: [reviews.rating_value desc]
    listen:
    -
    - Branch: branch.branch_address
      Date: reviews.published_date
    row: 5
    col: 16
    width: 7
    height: 5
  - name: Product Metrics
    title: Product Metrics
    model: block-keboola-bank_scorecard
    explore: scorecard_metrics
    type: looker_column
    fields: [scorecard_metrics.metric_no_of_product_credit, scorecard_metrics.metric_no_of_product_morgage,
      scorecard_metrics.metric_no_of_product_vehicle]
    filters:
      scorecard_metrics.metric: '"no_of_product_credit","no_of_product_morgage","no_of_product_vehicle"'
    sorts: [scorecard_metrics.metric_no_of_product_credit desc]
    limit: 500
    query_timezone: America/Los_Angeles
    color_application:
      collection_id: legacy
      palette_id: looker_classic
      options:
        steps: 5
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    label_value_format: "###,###"
    series_types: {}
    point_style: none
    series_colors:
      scorecard_metrics.metric_no_of_product_credit: "#33a02c"
      scorecard_metrics.metric_no_of_product_vehicle: "#90c8ae"
    series_labels:
      scorecard_metrics.metric_no_of_product_credit: "# of Credit Product"
      scorecard_metrics.metric_no_of_product_morgage: "# of Mortgage Product"
      scorecard_metrics.metric_no_of_product_vehicle: "# of Vehicle Product"
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    up_color: "#62bad4"
    down_color: "#a9c574"
    total_color: "#929292"
    listen:
      Branch: branch.branch_address
      Date: scorecard_metrics.date
    row: 29
    col: 9
    width: 14
    height: 6
  - name: Cross sell and up sell
    title: Cross sell and up sell
    model: block-keboola-bank_scorecard
    explore: scorecard_metrics
    type: looker_column
    fields: [scorecard_metrics.metric_no_of_cross_sells, scorecard_metrics.metric_no_of_upsells]
    limit: 500
    query_timezone: America/Los_Angeles
    color_application:
      collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7
      palette_id: 18d0c733-1d87-42a9-934f-4ba8ef81d736
      options:
        steps: 5
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    label_value_format: "###,###"
    point_style: none
    series_colors:
      scorecard_metrics.metric_no_of_cross_sells: "#fcd60f"
      scorecard_metrics.metric_no_of_upsells: "#FC9200"
    series_labels:
      scorecard_metrics.metric_no_of_cross_sells: "# of Cross Sells"
      scorecard_metrics.metric_no_of_upsells: "# of Up Sells"
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    listen:
      Branch: branch.branch_address
      Date: scorecard_metrics.date
    row: 23
    col: 9
    width: 7
    height: 6
  - name: Accounts
    title: Accounts
    model: block-keboola-bank_scorecard
    explore: scorecard_metrics
    type: looker_column
    fields: [scorecard_metrics.metric_no_of_consumer_accounts, scorecard_metrics.no_of_business_accounts]
    limit: 500
    query_timezone: America/Los_Angeles
    color_application:
      collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7
      palette_id: 18d0c733-1d87-42a9-934f-4ba8ef81d736
      options:
        steps: 5
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    label_value_format: "###,###"
    point_style: none
    series_colors:
      scorecard_metrics.metric_no_of_consumer_accounts: "#FC2E31"
      scorecard_metrics.no_of_business_accounts: "#3D52B9"
    series_labels:
      scorecard_metrics.metric_no_of_consumer_accounts: "# of Consumer Accounts"
      scorecard_metrics.no_of_business_accounts: "# of Business Accounts"
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    listen:
      Branch: branch.branch_address
      Date: scorecard_metrics.date
    row: 23
    col: 16
    width: 7
    height: 6
  - name: Average Customer Value
    title: Average Customer Value
    model: block-keboola-bank_scorecard
    explore: scorecard_metrics
    type: single_value
    fields: [scorecard_metrics.metric_avg_customer_value]
    limit: 500
    query_timezone: America/Los_Angeles
    custom_color_enabled: true
    show_single_value_title: true
    single_value_title: "∅ Customer Value"
    value_format: "$### ###"
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: scorecard_metrics.metric_no_of_customers,
            id: scorecard_metrics.metric_no_of_customers, name: Metric No of Customers}],
        showLabels: true, showValues: true, maxValue: 18500, minValue: 18000, unpinAxis: false,
        tickDensity: default, type: log}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    reference_lines: [{reference_type: line, line_value: mean, range_start: max, range_end: min,
        margin_top: deviation, margin_value: mean, margin_bottom: deviation, label_position: right,
        color: "#000000"}]
    show_null_points: true
    interpolation: linear
    listen:
      Branch: branch.branch_address
      Date: scorecard_metrics.date
    row: 32
    col: 4
    width: 5
    height: 3
  - name: Customer Health
    title: Customer Health
    model: block-keboola-bank_scorecard
    explore: scorecard_metrics
    type: single_value
    fields: [scorecard_metrics.metric_kpi_customer_health]
    limit: 500
    query_timezone: America/Los_Angeles
    color_application:
      collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd
      palette_id: b0768e0d-03b8-4c12-9e30-9ada6affc357
    custom_color_enabled: true
    custom_color: ''
    show_single_value_title: true
    single_value_title: Customer Health Index
    show_comparison: false
    comparison_type: change
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: true
    conditional_formatting: [{type: less than, value: 3, background_color: '', font_color: "#ff472e",
        color_application: {collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd, palette_id: 628a997f-dd44-4060-a913-250041880199},
        bold: false, italic: false, strikethrough: false, fields: !!null ''}, {type: between,
        value: [3, 6.5], background_color: '', font_color: "#ff9028", color_application: {
          collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd, palette_id: 628a997f-dd44-4060-a913-250041880199},
        bold: false, italic: false, strikethrough: false, fields: !!null ''}, {type: greater
          than, value: 6.5, background_color: '', font_color: "#2ccf12", color_application: {
          collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd, palette_id: 4dadd4d2-40af-451b-bcdd-1dfaedf76163},
        bold: false, italic: false, strikethrough: false, fields: !!null ''}]
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_dropoff: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    listen:
      Branch: branch.branch_address
      Date: scorecard_metrics.date
    row: 26
    col: 0
    width: 4
    height: 3
  - name: Satisfaction Survey
    title: Satisfaction Survey
    model: block-keboola-bank_scorecard
    explore: scorecard_metrics
    type: single_value
    fields: [scorecard_metrics.metric_kpi_employee_sat_survey]
    limit: 500
    query_timezone: America/Los_Angeles
    color_application:
      collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd
      palette_id: b0768e0d-03b8-4c12-9e30-9ada6affc357
    custom_color_enabled: true
    custom_color: ''
    show_single_value_title: true
    single_value_title: Employee Satisfaction Survey
    show_comparison: false
    comparison_type: change
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: true
    conditional_formatting: [{type: less than, value: 6.5, background_color: '', font_color: "#ff472e",
        color_application: {collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd, palette_id: 628a997f-dd44-4060-a913-250041880199},
        bold: false, italic: false, strikethrough: false, fields: !!null ''}, {type: between,
        value: [6.5, 8], background_color: '', font_color: "#ff9028", color_application: {
          collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd, palette_id: 628a997f-dd44-4060-a913-250041880199},
        bold: false, italic: false, strikethrough: false, fields: !!null ''}, {type: greater
          than, value: 8, background_color: '', font_color: "#2ccf12", color_application: {
          collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd, palette_id: 4dadd4d2-40af-451b-bcdd-1dfaedf76163},
        bold: false, italic: false, strikethrough: false, fields: !!null ''}]
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_dropoff: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    listen:
      Branch: branch.branch_address
      Date: scorecard_metrics.date
    row: 29
    col: 0
    width: 4
    height: 3
  - name: Customers
    title: Customers
    model: block-keboola-bank_scorecard
    explore: scorecard_metrics
    type: single_value
    fields: [scorecard_metrics.metric_no_of_customers]
    sorts: [scorecard_metrics.metric_no_of_customers desc]
    limit: 500
    query_timezone: America/Los_Angeles
    custom_color_enabled: true
    show_single_value_title: true
    single_value_title: "# of Customers"
    value_format: "### ###"
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: scorecard_metrics.metric_no_of_customers,
            id: scorecard_metrics.metric_no_of_customers, name: Metric No of Customers}],
        showLabels: true, showValues: true, maxValue: 18500, minValue: 18000, unpinAxis: false,
        tickDensity: default, type: log}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    reference_lines: [{reference_type: line, line_value: mean, range_start: max, range_end: min,
        margin_top: deviation, margin_value: mean, margin_bottom: deviation, label_position: right,
        color: "#000000"}]
    show_null_points: true
    interpolation: linear
    listen:
      Branch: branch.branch_address
      Date: scorecard_metrics.date
    row: 32
    col: 0
    width: 4
    height: 3
  - name: Digital Activations
    title: Digital Activations
    model: block-keboola-bank_scorecard
    explore: scorecard_metrics
    type: single_value
    fields: [scorecard_metrics.metric_kpi_digital_activations]
    limit: 500
    query_timezone: America/Los_Angeles
    color_application:
      collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd
      palette_id: b0768e0d-03b8-4c12-9e30-9ada6affc357
    custom_color_enabled: true
    custom_color: ''
    show_single_value_title: true
    single_value_title: Digital Activations
    show_comparison: false
    comparison_type: change
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: true
    conditional_formatting: [{type: less than, value: 30, background_color: '', font_color: "#ff472e",
        color_application: {collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd, palette_id: 628a997f-dd44-4060-a913-250041880199},
        bold: false, italic: false, strikethrough: false, fields: !!null ''}, {type: between,
        value: [30, 60], background_color: '', font_color: "#ff9028", color_application: {
          collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd, palette_id: 628a997f-dd44-4060-a913-250041880199},
        bold: false, italic: false, strikethrough: false, fields: !!null ''}, {type: greater
          than, value: 60, background_color: '', font_color: "#2ccf12", color_application: {
          collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd, palette_id: 4dadd4d2-40af-451b-bcdd-1dfaedf76163},
        bold: false, italic: false, strikethrough: false, fields: !!null ''}]
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_dropoff: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    listen:
      Branch: branch.branch_address
      Date: scorecard_metrics.date
    row: 23
    col: 0
    width: 4
    height: 3
  - name: Digital Activations Comparison
    title: Digital Activations Comparison
    merged_queries:
    - model: block-keboola-bank_scorecard
      explore: scorecard_metrics
      type: single_value
      fields: [scorecard_metrics.metric_kpi_digital_activations, branch.branch_address]
      sorts: [scorecard_metrics.metric_kpi_digital_activations desc]
      limit: 500
      column_limit: 50
      query_timezone: America/Los_Angeles
      color_application:
        collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd
        palette_id: b0768e0d-03b8-4c12-9e30-9ada6affc357
      custom_color_enabled: true
      custom_color: ''
      show_single_value_title: true
      single_value_title: Digital Activations
      show_comparison: false
      comparison_type: change
      comparison_reverse_colors: false
      show_comparison_label: true
      enable_conditional_formatting: true
      conditional_formatting: [{type: less than, value: 30, background_color: '',
          font_color: "#ff472e", color_application: {collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd,
            palette_id: 628a997f-dd44-4060-a913-250041880199}, bold: false, italic: false,
          strikethrough: false, fields: !!null ''}, {type: between, value: [30, 60],
          background_color: '', font_color: "#ff9028", color_application: {collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd,
            palette_id: 628a997f-dd44-4060-a913-250041880199}, bold: false, italic: false,
          strikethrough: false, fields: !!null ''}, {type: greater than, value: 60,
          background_color: '', font_color: "#2ccf12", color_application: {collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd,
            palette_id: 4dadd4d2-40af-451b-bcdd-1dfaedf76163}, bold: false, italic: false,
          strikethrough: false, fields: !!null ''}]
      conditional_formatting_include_totals: false
      conditional_formatting_include_nulls: false
      x_axis_gridlines: false
      y_axis_gridlines: true
      show_view_names: false
      show_y_axis_labels: true
      show_y_axis_ticks: true
      y_axis_tick_density: default
      y_axis_tick_density_custom: 5
      show_x_axis_label: true
      show_x_axis_ticks: true
      y_axis_scale_mode: linear
      x_axis_reversed: false
      y_axis_reversed: false
      plot_size_by_field: false
      trellis: ''
      stacking: ''
      limit_displayed_rows: false
      legend_position: center
      series_types: {}
      point_style: none
      show_value_labels: true
      label_density: 25
      x_axis_scale: auto
      y_axis_combined: true
      ordering: none
      show_null_labels: false
      show_dropoff: false
      show_totals_labels: false
      show_silhouette: false
      totals_color: "#808080"
    - model: block-keboola-bank_scorecard
      explore: scorecard_metrics
      type: table
      fields: [scorecard_metrics.metric_kpi_digital_activations, branch.branch_address]
      sorts: [scorecard_metrics.metric_kpi_digital_activations desc]
      limit: 500
      query_timezone: America/Los_Angeles
      join_fields:
      - field_name: branch.branch_address
        source_field_name: branch.branch_address
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: scorecard_metrics.metric_kpi_digital_activations,
            id: scorecard_metrics.metric_kpi_digital_activations, name: Scorecard
              Metrics}, {axisId: q1_scorecard_metrics.metric_kpi_digital_activations,
            id: q1_scorecard_metrics.metric_kpi_digital_activations, name: Scorecard
              Metrics}], showLabels: false, showValues: false, unpinAxis: false, tickDensity: default,
        tickDensityCustom: 5, type: linear}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: false
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    hide_legend: true
    legend_position: center
    series_types:
      scorecard_metrics.metric_kpi_digital_activations: area
    point_style: none
    series_colors:
      scorecard_metrics.metric_kpi_digital_activations: "#27a686"
      q1_scorecard_metrics.metric_kpi_digital_activations: "#ed6168"
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    reference_lines: [{reference_type: line, line_value: mean, range_start: max, range_end: min,
        margin_top: deviation, margin_value: mean, margin_bottom: deviation, label_position: right,
        color: "#27a686"}]
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    type: looker_column
    hidden_fields: [q1_branch.branch_address]
    sorts: [scorecard_metrics.metric_kpi_digital_activations desc]
    listen:
    - Date: scorecard_metrics.date
    - Branch: branch.branch_address
      Date: scorecard_metrics.date
    row: 23
    col: 4
    width: 5
    height: 3
  - name: Health Index Comparison
    title: Health Index Comparison
    merged_queries:
    - model: block-keboola-bank_scorecard
      explore: scorecard_metrics
      type: single_value
      fields: [branch.branch_address, scorecard_metrics.metric_kpi_customer_health]
      limit: 500
      column_limit: 50
      query_timezone: America/Los_Angeles
      color_application:
        collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd
        palette_id: b0768e0d-03b8-4c12-9e30-9ada6affc357
      custom_color_enabled: true
      custom_color: ''
      show_single_value_title: true
      single_value_title: Digital Activations
      show_comparison: false
      comparison_type: change
      comparison_reverse_colors: false
      show_comparison_label: true
      enable_conditional_formatting: true
      conditional_formatting: [{type: less than, value: 30, background_color: '',
          font_color: "#ff472e", color_application: {collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd,
            palette_id: 628a997f-dd44-4060-a913-250041880199}, bold: false, italic: false,
          strikethrough: false, fields: !!null ''}, {type: between, value: [30, 60],
          background_color: '', font_color: "#ff9028", color_application: {collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd,
            palette_id: 628a997f-dd44-4060-a913-250041880199}, bold: false, italic: false,
          strikethrough: false, fields: !!null ''}, {type: greater than, value: 60,
          background_color: '', font_color: "#2ccf12", color_application: {collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd,
            palette_id: 4dadd4d2-40af-451b-bcdd-1dfaedf76163}, bold: false, italic: false,
          strikethrough: false, fields: !!null ''}]
      conditional_formatting_include_totals: false
      conditional_formatting_include_nulls: false
      x_axis_gridlines: false
      y_axis_gridlines: true
      show_view_names: false
      show_y_axis_labels: true
      show_y_axis_ticks: true
      y_axis_tick_density: default
      y_axis_tick_density_custom: 5
      show_x_axis_label: true
      show_x_axis_ticks: true
      y_axis_scale_mode: linear
      x_axis_reversed: false
      y_axis_reversed: false
      plot_size_by_field: false
      trellis: ''
      stacking: ''
      limit_displayed_rows: false
      legend_position: center
      series_types: {}
      point_style: none
      show_value_labels: true
      label_density: 25
      x_axis_scale: auto
      y_axis_combined: true
      ordering: none
      show_null_labels: false
      show_dropoff: false
      show_totals_labels: false
      show_silhouette: false
      totals_color: "#808080"
    - model: block-keboola-bank_scorecard
      explore: scorecard_metrics
      type: table
      fields: [branch.branch_address, scorecard_metrics.metric_kpi_customer_health]
      sorts: [scorecard_metrics.metric_kpi_customer_health desc]
      limit: 500
      query_timezone: America/Los_Angeles
      join_fields:
      - field_name: branch.branch_address
        source_field_name: branch.branch_address
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: scorecard_metrics.metric_kpi_digital_activations,
            id: scorecard_metrics.metric_kpi_digital_activations, name: Scorecard
              Metrics}, {axisId: q1_scorecard_metrics.metric_kpi_digital_activations,
            id: q1_scorecard_metrics.metric_kpi_digital_activations, name: Scorecard
              Metrics}], showLabels: false, showValues: false, unpinAxis: false, tickDensity: default,
        tickDensityCustom: 5, type: linear}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: false
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    hide_legend: true
    legend_position: center
    series_types:
      scorecard_metrics.metric_kpi_digital_activations: area
      scorecard_metrics.metric_kpi_customer_health: area
    point_style: none
    series_colors:
      scorecard_metrics.metric_kpi_digital_activations: "#27a686"
      q1_scorecard_metrics.metric_kpi_digital_activations: "#ed6168"
      scorecard_metrics.metric_kpi_customer_health: "#27a686"
      q1_scorecard_metrics.metric_kpi_customer_health: "#ed6168"
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    reference_lines: [{reference_type: line, line_value: mean, range_start: max, range_end: min,
        margin_top: deviation, margin_value: mean, margin_bottom: deviation, label_position: right,
        color: "#27a686"}]
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    type: looker_column
    hidden_fields: [q1_branch.branch_address]
    listen:
    - Date: scorecard_metrics.date
    - Branch: branch.branch_address
      Date: scorecard_metrics.date
    row: 26
    col: 4
    width: 5
    height: 3
  - name: Employee Sat Comparison
    title: Employee Sat. Comparison
    merged_queries:
    - model: block-keboola-bank_scorecard
      explore: scorecard_metrics
      type: single_value
      fields: [branch.branch_address, scorecard_metrics.metric_kpi_employee_sat_survey]
      limit: 500
      column_limit: 50
      query_timezone: America/Los_Angeles
      color_application:
        collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd
        palette_id: b0768e0d-03b8-4c12-9e30-9ada6affc357
      custom_color_enabled: true
      custom_color: ''
      show_single_value_title: true
      single_value_title: Digital Activations
      show_comparison: false
      comparison_type: change
      comparison_reverse_colors: false
      show_comparison_label: true
      enable_conditional_formatting: true
      conditional_formatting: [{type: less than, value: 30, background_color: '',
          font_color: "#ff472e", color_application: {collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd,
            palette_id: 628a997f-dd44-4060-a913-250041880199}, bold: false, italic: false,
          strikethrough: false, fields: !!null ''}, {type: between, value: [30, 60],
          background_color: '', font_color: "#ff9028", color_application: {collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd,
            palette_id: 628a997f-dd44-4060-a913-250041880199}, bold: false, italic: false,
          strikethrough: false, fields: !!null ''}, {type: greater than, value: 60,
          background_color: '', font_color: "#2ccf12", color_application: {collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd,
            palette_id: 4dadd4d2-40af-451b-bcdd-1dfaedf76163}, bold: false, italic: false,
          strikethrough: false, fields: !!null ''}]
      conditional_formatting_include_totals: false
      conditional_formatting_include_nulls: false
      x_axis_gridlines: false
      y_axis_gridlines: true
      show_view_names: false
      show_y_axis_labels: true
      show_y_axis_ticks: true
      y_axis_tick_density: default
      y_axis_tick_density_custom: 5
      show_x_axis_label: true
      show_x_axis_ticks: true
      y_axis_scale_mode: linear
      x_axis_reversed: false
      y_axis_reversed: false
      plot_size_by_field: false
      trellis: ''
      stacking: ''
      limit_displayed_rows: false
      legend_position: center
      series_types: {}
      point_style: none
      show_value_labels: true
      label_density: 25
      x_axis_scale: auto
      y_axis_combined: true
      ordering: none
      show_null_labels: false
      show_dropoff: false
      show_totals_labels: false
      show_silhouette: false
      totals_color: "#808080"
    - model: block-keboola-bank_scorecard
      explore: scorecard_metrics
      type: table
      fields: [branch.branch_address, scorecard_metrics.metric_kpi_employee_sat_survey]
      limit: 500
      query_timezone: America/Los_Angeles
      join_fields:
      - field_name: branch.branch_address
        source_field_name: branch.branch_address
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: '', orientation: left, series: [{axisId: scorecard_metrics.metric_kpi_digital_activations,
            id: scorecard_metrics.metric_kpi_digital_activations, name: Scorecard
              Metrics}, {axisId: q1_scorecard_metrics.metric_kpi_digital_activations,
            id: q1_scorecard_metrics.metric_kpi_digital_activations, name: Scorecard
              Metrics}], showLabels: false, showValues: false, unpinAxis: false, tickDensity: default,
        tickDensityCustom: 5, type: linear}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: false
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    hide_legend: true
    legend_position: center
    series_types:
      scorecard_metrics.metric_kpi_digital_activations: area
      scorecard_metrics.metric_kpi_customer_health: area
      scorecard_metrics.metric_kpi_employee_sat_survey: area
    point_style: none
    series_colors:
      scorecard_metrics.metric_kpi_digital_activations: "#27a686"
      q1_scorecard_metrics.metric_kpi_digital_activations: "#ed6168"
      scorecard_metrics.metric_kpi_customer_health: "#27a686"
      q1_scorecard_metrics.metric_kpi_customer_health: "#ed6168"
      scorecard_metrics.metric_kpi_employee_sat_survey: "#27a686"
      q1_scorecard_metrics.metric_kpi_employee_sat_survey: "#ed6168"
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    reference_lines: [{reference_type: line, line_value: mean, range_start: max, range_end: min,
        margin_top: deviation, margin_value: mean, margin_bottom: deviation, label_position: right,
        color: "#27a686"}]
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    type: looker_column
    hidden_fields: [q1_branch.branch_address]
    sorts: [scorecard_metrics.metric_kpi_employee_sat_survey desc]
    listen:
    - Date: scorecard_metrics.date
    - Branch: branch.branch_address
      Date: scorecard_metrics.date
    row: 29
    col: 4
    width: 5
    height: 3
  filters:
  - name: Branch
    title: Branch
    type: field_filter
    default_value: 1 Commerce Way
    allow_multiple_values: false
    required: true
    model: block-keboola-bank_scorecard
    explore: branch
    listens_to_filters: []
    field: branch.branch_address
  - name: Date
    title: Date
    type: date_filter
    default_value: 2019/09/01
    allow_multiple_values: true
    required: false