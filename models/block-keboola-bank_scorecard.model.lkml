connection: "keboola_block_bank_scorecard"

# include all the views
include: "/views/**/*.view"

# include all lookml dashboards
include: "/*.dashboard.lookml"

datagroup: block_keboola_bank_scorecard_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: block_keboola_bank_scorecard_default_datagroup

explore: branch {}

explore: customer_metrics {
  join: branch {
    type: left_outer
    sql_on: ${customer_metrics.branch_id} = ${branch.branch_id} ;;
    relationship: many_to_one
  }

  join: customers {
    type: left_outer
    sql_on: ${customer_metrics.customer_id} = ${customers.customer_id} ;;
    relationship: many_to_one
  }
}

explore: customers {
  join: branch {
    type: left_outer
    sql_on: ${customers.branch_id} = ${branch.branch_id} ;;
    relationship: many_to_one
  }
}

explore: interactions {
  join: customers {
    type: left_outer
    sql_on: ${interactions.customer_id} = ${customers.customer_id} ;;
    relationship: many_to_one
  }

  join: branch {
    type: left_outer
    sql_on: ${interactions.branch_id} = ${branch.branch_id} ;;
    relationship: many_to_one
  }

  join: product {
    type: left_outer
    sql_on: ${interactions.product_id} = ${product.product_id} ;;
    relationship: many_to_one
  }
}

explore: main_branch {
  join: customers {
    type: left_outer
    sql_on: ${main_branch.customer_id} = ${customers.customer_id} ;;
    relationship: many_to_one
  }

  join: branch {
    type: left_outer
    sql_on: ${main_branch.branch_id} = ${branch.branch_id} ;;
    relationship: many_to_one
  }
}

explore: product {}

explore: product_events {
  join: product {
    type: left_outer
    sql_on: ${product_events.product_id} = ${product.product_id} ;;
    relationship: many_to_one
  }

  join: customers {
    type: left_outer
    sql_on: ${product_events.customer_id} = ${customers.customer_id} ;;
    relationship: many_to_one
  }

  join: branch {
    type: left_outer
    sql_on: ${customers.branch_id} = ${branch.branch_id} ;;
    relationship: many_to_one
  }
}

explore: product_usage {
  join: product {
    type: left_outer
    sql_on: ${product_usage.product_id} = ${product.product_id} ;;
    relationship: many_to_one
  }

  join: customers {
    type: left_outer
    sql_on: ${product_usage.customer_id} = ${customers.customer_id} ;;
    relationship: many_to_one
  }

  join: branch {
    type: left_outer
    sql_on: ${customers.branch_id} = ${branch.branch_id} ;;
    relationship: many_to_one
  }
}

explore: reviews {
  join: interactions {
    type: left_outer
    sql_on: ${reviews.interaction_id} = ${interactions.interaction_id} ;;
    relationship: many_to_one
  }

  join: customers {
    type: left_outer
    sql_on: ${interactions.customer_id} = ${customers.customer_id} ;;
    relationship: many_to_one
  }

  join: branch {
    type: left_outer
    sql_on: ${interactions.branch_id} = ${branch.branch_id} ;;
    relationship: many_to_one
  }

  join: product {
    type: left_outer
    sql_on: ${interactions.product_id} = ${product.product_id} ;;
    relationship: many_to_one
  }
}

explore: reviews_topic {
  join: topic {
    type: left_outer
    sql_on: ${reviews_topic.topic_id} = ${topic.topic_id} ;;
    relationship: many_to_one
  }
  join: reviews {
    type: left_outer
    sql_on: ${reviews_topic.review_response_id} = ${reviews.review_id} ;;
    relationship: many_to_one
  }

  join: interactions {
    type: left_outer
    sql_on: ${reviews.interaction_id} = ${interactions.interaction_id} ;;
    relationship: many_to_one
  }

  join: branch {
    type: left_outer
    sql_on: ${interactions.branch_id} = ${branch.branch_id} ;;
    relationship: many_to_one
  }
}

explore: scorecard_metrics {
  join: branch {
    type: left_outer
    sql_on: ${scorecard_metrics.branch_id} = ${branch.branch_id} ;;
    relationship: many_to_one
  }
}

explore: topic {}
