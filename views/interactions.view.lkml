view: interactions {
  sql_table_name: WORKSPACE_564504055.INTERACTIONS ;;
  drill_fields: [interaction_id]

  dimension: interaction_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."INTERACTION_ID" ;;
  }

  dimension: branch_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."BRANCH_ID" ;;
  }

  dimension: customer_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."CUSTOMER_ID" ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."DATE" ;;
  }

  dimension: key {
    type: string
    sql: ${TABLE}."KEY" ;;
  }

  dimension: product_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."PRODUCT_ID" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."TYPE" ;;
  }

  dimension: value {
    type: number
    sql: ${TABLE}."VALUE" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      interaction_id,
      customers.customer_id,
      customers.first_name,
      customers.last_name,
      branch.branch_id,
      product.name,
      product.product_id,
      reviews.count
    ]
  }
}
