view: main_branch {
  sql_table_name: WORKSPACE_564504055.MAIN_BRANCH ;;

  dimension: branch_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."BRANCH_ID" ;;
  }

  dimension: customer_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."CUSTOMER_ID" ;;
  }

  measure: count {
    type: count
    drill_fields: [customers.customer_id, customers.first_name, customers.last_name, branch.branch_id]
  }
}
