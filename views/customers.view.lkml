view: customers {
  sql_table_name: WORKSPACE_564504055.CUSTOMERS ;;
  drill_fields: [customer_id]

  dimension: customer_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."CUSTOMER_ID" ;;
  }

  dimension: age {
    type: number
    sql: ${TABLE}."AGE" ;;
  }

  dimension: age_group {
    type: string
    sql: ${TABLE}."AGE_GROUP" ;;
  }

  dimension: balance {
    type: number
    sql: ${TABLE}."BALANCE" ;;
  }

  dimension: branch_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."BRANCH_ID" ;;
  }

  dimension: credit_score {
    type: number
    sql: ${TABLE}."CREDIT_SCORE" ;;
  }

  dimension_group: date_created {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."DATE_CREATED" ;;
  }

  dimension_group: date_terminated {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."DATE_TERMINATED" ;;
  }

  dimension: estimated_salary {
    type: number
    sql: ${TABLE}."ESTIMATED_SALARY" ;;
  }

  dimension: exited {
    type: string
    sql: ${TABLE}."EXITED" ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}."FIRST_NAME" ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}."GENDER" ;;
  }

  dimension: geography {
    type: string
    sql: ${TABLE}."GEOGRAPHY" ;;
  }

  dimension: has_cr_card {
    type: string
    sql: ${TABLE}."HAS_CR_CARD" ;;
  }

  dimension: is_active_member {
    type: string
    sql: ${TABLE}."IS_ACTIVE_MEMBER" ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}."LAST_NAME" ;;
  }

  dimension: num_of_products {
    type: number
    sql: ${TABLE}."NUM_OF_PRODUCTS" ;;
  }

  dimension: salary_group {
    type: string
    sql: ${TABLE}."SALARY_GROUP" ;;
  }

  dimension: segment_id {
    type: string
    sql: ${TABLE}."SEGMENT_ID" ;;
  }

  dimension: tenure {
    type: number
    sql: ${TABLE}."TENURE" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      customer_id,
      first_name,
      last_name,
      branch.branch_id,
      customer_metrics.count,
      interactions.count,
      main_branch.count,
      product_events.count,
      product_usage.count
    ]
  }
}
