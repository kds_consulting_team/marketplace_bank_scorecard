view: reviews {
  sql_table_name: WORKSPACE_564504055.REVIEWS ;;
  drill_fields: [review_id]

  dimension: review_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."REVIEW_ID" ;;
  }

  dimension: interaction_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."INTERACTION_ID" ;;
  }

  dimension: review_rating {
    type: number
    sql: ${TABLE}."REVIEW_RATING" ;;
  }

  dimension: review_response_id {
    type: string
    sql: ${TABLE}."REVIEW_RESPONSE_ID" ;;
  }

  dimension: sentiment_label {
    type: string
    sql: ${TABLE}."REVIEW_SENTIMENT_LABEL" ;;
  }

  dimension: sentiment_value_dimension {
    type: number
    sql: ${TABLE}."REVIEW_SENTIMENT_VALUE" ;;
  }

  dimension: review_source {
    type: string
    sql: ${TABLE}."REVIEW_SOURCE" ;;
  }

  dimension: review_text {
    type: string
    sql: ${TABLE}."REVIEW_TEXT" ;;
  }

  dimension: review_text_length {
    type: number
    sql: ${TABLE}."REVIEW_TEXT_LENGTH" ;;
  }

  dimension: review_title {
    type: string
    sql: ${TABLE}."REVIEW_TITLE" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."REVIEW_TYPE" ;;
  }

  dimension: source_url {
    type: string
    sql: ${TABLE}."SOURCE_URL" ;;
  }

  dimension_group: published {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."PUBLISHED_AT" ;;
  }
  measure: rating_value {
    type: average
    sql: ${TABLE}."RATING" ;;
    value_format: "# ##0.0#"
    drill_fields: [location.location, published_date, source, text, rating_stars, sentiment_label, rating_value]
  }

  measure: recent_rating {
    type: average
    sql: ${TABLE}."RATING" ;;
    filters: {
      field: published_date
      value: "last 14 days"
    }
    view_label: ""
    value_format: "# ##0.0#"
    drill_fields: [location.location, published_date, source, text, rating_stars, sentiment_label, recent_rating]
  }

  measure: old_rating {
    description: "14 days ago"
    type: average
    sql: ${TABLE}."RATING" ;;
    filters: {
      field: published_date
      value: "before 14 days ago"
    }
    view_label: ""
    value_format: "# ##0.0#"
    drill_fields: [location.location, published_date, source, text, rating_stars, sentiment_label, old_rating]
  }

  measure: rating_stars {
    type: average
    sql: ${TABLE}."RATING" ;;
    view_label: ""
    html:
      {%if {{value}} > 0.5%}
        {%if {{value}} > 1.5%}
          {%if {{value}} > 2.5%}
            {%if {{value}} > 3.5%}
              {%if {{value}} > 4.5%}
              ✪ ✪ ✪ ✪ ✪
              {%else%}
              ✪ ✪ ✪ ✪
              {%endif%}
            {%else%}
            ✪ ✪ ✪
            {%endif%}
          {%else%}
          ✪ ✪
          {%endif%}
        {%else%}
        ✪
        {%endif%}
      {%else%}
      💩
      {%endif%};;
  }

  measure: sentiment_value {
    type: average
    sql: ${sentiment_value_dimension};;
    value_format: "# ##0.0#"
    drill_fields: [location.location, published_date, source, text, rating_stars, sentiment_label, sentiment_value]
  }

  measure: review_sentiment_value {
    type: average
    sql: ${sentiment_value_dimension} ;;
    value_format: "# ##0.0#"
    filters: {
      field: type
      value: "Review"
    }
    drill_fields: [location.location, published_date, source, text, rating_stars, sentiment_label, review_sentiment_value]
  }

  measure: response_sentiment_value {
    type: average
    sql: ${TABLE}."SENTIMENT_VALUE" ;;
    value_format: "# ##0.0#"
    filters: {
      field: type
      value: "Response"
    }
    drill_fields: [location.location, published_date, source, text, rating_stars, sentiment_label, response_sentiment_value]
  }

  dimension: source {
    type: string
    sql: ${TABLE}."SOURCE" ;;
    html: <a href={{source_url}} target="_blank"><font color="blue">{{ value }}</font></a> ;;
  }


  dimension: text {
    type: string
    sql: ${TABLE}."TEXT" ;;
  }

  dimension: text_short {
    type: string
    sql: IFF(LENGTH(${TABLE}."TEXT")>256,LEFT(${TABLE}."TEXT",253)||'...',${TABLE}."TEXT") ;;
    drill_fields: [text]
  }

  measure: text_length {
    type: sum
    sql: ${review_text_length} ;;
  }

  measure: count_total {
    type: count
    drill_fields: [reviews_list*, count_total]
  }

  measure: count_reviews {
    type: count_distinct
    sql: ${review_id} ;;
    drill_fields: [reviews_list*, count_reviews]
  }

  measure: count_responses {
    type: count_distinct
    sql: ${review_response_id} ;;
    filters: {
      field: type
      value: "Response"
    }
    drill_fields: [reviews_list*, count_reviews]
  }

  measure: recent_count_reviews {
    type: count
    drill_fields: [reviews_list*, recent_count_reviews]
    label: "Number of Recent Reviews"
    view_label: ""
    filters: {
      field: published_date
      value: "last 14 days"
    }
  }

  set: reviews_list {
    fields: [branch.address, published_date, source, text, rating_stars, sentiment_label]
  }
}
