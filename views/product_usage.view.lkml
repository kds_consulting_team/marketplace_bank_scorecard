view: product_usage {
  sql_table_name: WORKSPACE_564504055.PRODUCT_USAGE ;;
  drill_fields: [product_usage_id]

  dimension: product_usage_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."PRODUCT_USAGE_ID" ;;
  }

  dimension: customer_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."CUSTOMER_ID" ;;
  }

  dimension_group: date_activated {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."DATE_ACTIVATED" ;;
  }

  dimension_group: date_deactivated {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."DATE_DEACTIVATED" ;;
  }

  dimension: product_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."PRODUCT_ID" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      product_usage_id,
      product.name,
      product.product_id,
      customers.customer_id,
      customers.first_name,
      customers.last_name
    ]
  }
}
