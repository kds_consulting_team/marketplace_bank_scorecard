view: scorecard_metrics {
  sql_table_name: WORKSPACE_564504055.scorecard_metrics ;;

  dimension: address {
    type: string
    sql: ${TABLE}."ADDRESS" ;;
  }

  dimension: age_group {
    type: string
    sql: ${TABLE}."AGE_GROUP" ;;
  }

  dimension: branch_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."BRANCH_ID" ;;
  }

  dimension_group: scorecard_date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."DATE" ;;
  }

  dimension: date {
    type: date
    sql: ${TABLE}."DATE" ;;
  }

  dimension: metric {
    type: string
    sql: ${TABLE}."METRIC" ;;
  }

  dimension: metric_type {
    type: string
    sql: ${TABLE}."METRIC_TYPE" ;;
  }

  dimension: metric_value_original {
    type: number
    sql: ${TABLE}."METRIC_VALUE" ;;
  }

  dimension: metric_value_normalized {
    type: number
    sql: ${TABLE}."METRIC_VALUE_NORMALIZED" ;;
  }

  dimension: operation {
    type: string
    sql: ${TABLE}."OPERATION" ;;
  }

  dimension: rn {
    type: number
    sql: ${TABLE}."RN" ;;
  }

  dimension: segment {
    type: string
    sql: ${TABLE}."SEGMENT" ;;
  }

  dimension: wealth_group {
    type: string
    sql: ${TABLE}."WEALTH_GROUP" ;;
  }

  measure: count {
    type: count
    drill_fields: [branch.branch_id]
  }

  measure: metric_value{
    type: number
    sql: IFF(${operation}='SUM',SUM(${TABLE}."METRIC_VALUE"),AVG(${TABLE}."METRIC_VALUE")) ;;
  }

  measure: metric_avg_customer_value{
    type: average
    sql: ${metric_value_original} ;;
    filters: {
      field:  metric
      value: "avg_customer_value"
    }
  }

  measure: metric_no_of_product_vehicle {
    type: sum
    sql: ${metric_value_original} ;;
    filters: {
      field: metric
      value: "no_of_product_vehicle"
    }
  }

  measure: metric_no_of_product_credit {
    type: sum
    sql: ${metric_value_original} ;;
    filters: {
      field: metric
      value: "no_of_product_credit"
    }
  }

  measure: metric_kpi_digital_activations {
    type: average
    sql: ${metric_value_original} ;;
    value_format: "0.00"
    filters: {
      field: metric
      value: "kpi_digital_activations"
    }
  }

  measure: metric_no_of_cross_sells {
    type: sum
    sql: ${metric_value_original} ;;
    filters: {
      field:  metric
      value: "no_of_cross_sells"
    }
  }

  measure: metric_no_of_customers {
    type: sum
    sql: ${metric_value_original} ;;
    value_format: "0"
    filters: {
      field: metric
      value: "no_of_customers"
    }
  }

  measure: metric_kpi_customer_health {
    type: average
    sql: ${metric_value_original} ;;
    value_format: "0.00"
    filters: {
      field:metric
      value: "kpi_customer_health"}
  }

  measure: metric_no_of_product_morgage {
    type: sum
    sql: ${metric_value_original} ;;
    filters: {
      field: metric
      value: "no_of_product_morgage"
    }
  }

  measure: metric_no_of_upsells {
    type: sum
    sql: ${metric_value_original} ;;
    filters: {
      field: metric
      value: "no_of_upsells"
    }
  }

  measure: metric_kpi_employee_sat_survey {
    type: average
    sql: ${metric_value_original} ;;
    value_format: "0.00"
    filters: {
      field: metric
      value: "kpi_employee_sat_survey"
    }
  }

  measure: metric_no_of_consumer_accounts {
    type: sum
    sql: ${metric_value_original} ;;
    filters: {
      field: metric
      value: "no_of_consumer_accounts"
    }
  }

  measure: no_of_business_accounts {
    type: sum
    sql: ${metric_value_original} ;;
    filters: {
      field: metric
      value: "no_of_business_accounts"
    }
  }


}
