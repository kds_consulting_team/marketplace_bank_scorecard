view: reviews_topic {
  sql_table_name: WORKSPACE_564504055.REVIEWS_TOPIC ;;

  dimension: review_response_id {
    type: string
    sql: ${TABLE}."REVIEW_RESPONSE_ID" ;;
  }

  dimension: review_response_topic_id {
    type: string
    sql: ${TABLE}."REVIEW_RESPONSE_TOPIC_ID" ;;
  }

  dimension: topic_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."TOPIC_ID" ;;
  }

  dimension: topic_sentiment_label {
    type: string
    sql: ${TABLE}."TOPIC_SENTIMENT_LABEL" ;;
  }

  dimension: topic_sentiment_value {
    type: number
    sql: ${TABLE}."TOPIC_SENTIMENT_VALUE" ;;
  }

  measure: count {
    type: count
    drill_fields: [topic.topic_id]
  }
}
