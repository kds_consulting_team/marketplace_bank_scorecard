view: product {
  sql_table_name: WORKSPACE_564504055.PRODUCT ;;
  drill_fields: [product_id]

  dimension: product_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."PRODUCT_ID" ;;
  }

  dimension: category {
    type: string
    sql: ${TABLE}."CATEGORY" ;;
  }

  dimension: code {
    type: string
    sql: ${TABLE}."CODE" ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}."DESCRIPTION" ;;
  }

  dimension: details {
    type: string
    sql: ${TABLE}."DETAILS" ;;
  }

  dimension: family {
    type: string
    sql: ${TABLE}."FAMILY" ;;
  }

  dimension: more_info_url {
    type: string
    sql: ${TABLE}."MORE_INFO_URL" ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}."NAME" ;;
  }

  dimension: parent_product_code {
    type: string
    sql: ${TABLE}."PARENT_PRODUCT_CODE" ;;
  }

  dimension: super_family {
    type: string
    sql: ${TABLE}."SUPER_FAMILY" ;;
  }

  measure: count {
    type: count
    drill_fields: [product_id, name, interactions.count, product_events.count, product_usage.count]
  }
}
