view: product_events {
  sql_table_name: WORKSPACE_564504055.PRODUCT_EVENTS ;;
  drill_fields: [product_events_id]

  dimension: product_events_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."PRODUCT_EVENTS_ID" ;;
  }

  dimension: customer_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."CUSTOMER_ID" ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."DATE" ;;
  }

  dimension: is_active {
    type: number
    sql: ${TABLE}."IS_ACTIVE" ;;
  }

  dimension: product_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."PRODUCT_ID" ;;
  }

  dimension: product_order {
    type: number
    sql: ${TABLE}."PRODUCT_ORDER" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."TYPE" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      product_events_id,
      product.name,
      product.product_id,
      customers.customer_id,
      customers.first_name,
      customers.last_name
    ]
  }
}
