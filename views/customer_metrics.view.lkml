view: customer_metrics {
  sql_table_name: WORKSPACE_564504055.CUSTOMER_METRICS ;;

  dimension: actual {
    type: string
    sql: ${TABLE}."ACTUAL" ;;
  }

  dimension: age_group {
    type: string
    sql: ${TABLE}."AGE_GROUP" ;;
  }

  dimension: branch_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."BRANCH_ID" ;;
  }

  dimension: credit_score {
    type: number
    sql: ${TABLE}."CREDIT_SCORE" ;;
  }

  dimension: customer_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."CUSTOMER_ID" ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."END_DATE" ;;
  }

  dimension: estimated_salary {
    type: number
    sql: ${TABLE}."ESTIMATED_SALARY" ;;
  }

  dimension: exited {
    type: string
    sql: ${TABLE}."EXITED" ;;
  }

  dimension: is_deleted {
    type: string
    sql: ${TABLE}."IS_DELETED" ;;
  }

  dimension: salary_group {
    type: string
    sql: ${TABLE}."SALARY_GROUP" ;;
  }

  dimension: segment_id {
    type: string
    sql: ${TABLE}."SEGMENT_ID" ;;
  }

  dimension: snap_pk {
    type: string
    sql: ${TABLE}."SNAP_PK" ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."START_DATE" ;;
  }

  measure: count {
    type: count
    drill_fields: [branch.branch_id, customers.customer_id, customers.first_name, customers.last_name]
  }
}
