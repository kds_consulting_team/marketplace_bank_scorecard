view: topic {
  sql_table_name: WORKSPACE_564504055.TOPIC ;;
  drill_fields: [topic_id]

  dimension: topic_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."TOPIC_ID" ;;
  }

  dimension: topic {
    type: string
    sql: ${TABLE}."TOPIC" ;;
  }

  dimension: topic_category_type {
    type: string
    sql: ${TABLE}."TOPIC_CATEGORY_TYPE" ;;
  }

  dimension: topic_group {
    type: string
    sql: ${TABLE}."TOPIC_GROUP" ;;
  }

  dimension: topic_object {
    type: string
    sql: ${TABLE}."TOPIC_OBJECT" ;;
  }

  dimension: topic_subject {
    type: string
    sql: ${TABLE}."TOPIC_SUBJECT" ;;
  }

  dimension: topic_type {
    type: string
    sql: ${TABLE}."TOPIC_TYPE" ;;
  }

  measure: count {
    type: count
    drill_fields: [topic_id, reviews_topic.count]
  }
}
