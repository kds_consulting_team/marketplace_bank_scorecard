view: branch {
  sql_table_name: WORKSPACE_564504055.BRANCH ;;
  drill_fields: [branch_id]

  dimension: branch_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."BRANCH_ID" ;;
  }

  dimension: bank_id {
    type: string
    sql: ${TABLE}."BANK_ID" ;;
  }

  dimension: branch_address {
    type: string
    sql: ${TABLE}."BRANCH_ADDRESS" ;;
  }

  dimension: branch_city {
    type: string
    sql: ${TABLE}."BRANCH_CITY" ;;
  }

  dimension: branch_country {
    type: string
    sql: ${TABLE}."BRANCH_COUNTRY" ;;
  }

  dimension: branch_country_id {
    type: string
    sql: ${TABLE}."BRANCH_COUNTRY_ID" ;;
  }

  dimension: branch_phone {
    type: string
    sql: ${TABLE}."BRANCH_PHONE" ;;
  }

  dimension: branch_state {
    type: string
    sql: ${TABLE}."BRANCH_STATE" ;;
  }

  dimension: branch_zipcode {
    type: string
    sql: ${TABLE}."BRANCH_ZIPCODE" ;;
  }

  dimension: google_place_id {
    type: string
    sql: ${TABLE}."GOOGLE_PLACE_ID" ;;
  }

  dimension: latitude {
    type: string
    sql: ${TABLE}."LATITUDE" ;;
  }

  dimension: longitude {
    type: string
    sql: ${TABLE}."LONGITUDE" ;;
  }

  dimension: r_trackers_id {
    type: string
    sql: ${TABLE}."R_TRACKERS_ID" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: location_gps {
    type: location
    label: "Location GPS"
    sql_latitude:${TABLE}."LATITUDE" ;;
    sql_longitude:${TABLE}."LONGITUDE" ;;
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      branch_id,
      customers.count,
      customer_metrics.count,
      interactions.count,
      main_branch.count,
      scorecard_metrics.count
    ]
  }
}
